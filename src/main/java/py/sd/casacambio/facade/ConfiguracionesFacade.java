/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.casacambio.facade;

import javax.ejb.Stateless;
import py.sd.casacambio.mappers.ConfiguracionesMapper;
import py.sd.casacambio.pojos.Configuraciones;
import py.sd.casacambio.pojos.ConfiguracionesExample;

/**
 *
 * @author Miguel Fernandez <myguelfernandez@gmail.com>
 */
@Stateless
public class ConfiguracionesFacade extends GenericFacade<Configuraciones, Long, ConfiguracionesExample, ConfiguracionesMapper>{
    
}
