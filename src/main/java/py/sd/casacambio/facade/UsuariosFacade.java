/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.casacambio.facade;

import javax.ejb.Stateless;
import py.sd.casacambio.mappers.UsuariosMapper;
import py.sd.casacambio.pojos.Usuarios;
import py.sd.casacambio.pojos.UsuariosExample;


/**
 *
 * @author Miguel Fernandez <myguelfernandez@gmail.com>
 */
@Stateless
public class UsuariosFacade extends GenericFacade<Usuarios, Long, UsuariosExample, UsuariosMapper> {


}
