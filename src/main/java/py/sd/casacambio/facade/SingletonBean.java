/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.casacambio.facade;

import java.io.InputStream;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import static javax.ejb.TransactionManagementType.BEAN;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;



/**
 *
 * @author Miguel Fernandez <myguelfernandez@gmail.com>
 */
@Singleton
@Startup
@TransactionManagement(BEAN)
public class SingletonBean {

    private final Logger logger = LogManager.getLogger(this.getClass());

    

    @Resource
    UserTransaction userTransaction;

    @PostConstruct
    public void init() {

        try {
            System.out.println("init");
            InputStream log4j2 = this.getClass().getClassLoader().getResourceAsStream("log4j2.xml");
            System.out.println("log " + log4j2);
            ConfigurationSource source = new ConfigurationSource(log4j2);
            Configurator.initialize(null, source);
        } catch (Exception e) {
            System.out.println("ERROR");
            e.printStackTrace();
        }



    }
}
