/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.casacambio.facade;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author Miguel Fernandez <myguelfernandez@gmail.com>
 * @param <P> PojoClass
 * @param <ID> PrimaryKeyClass
 * @param <E> ExampleClass
 * @param <M> MapperClass
 */
public abstract class GenericFacade<P, ID extends Serializable, E, M> {

    private final Logger logger = LogManager.getLogger(this.getClass());
    private final Class<P> pojoClass;
    private final Class<E> exampleClass;
    private final Class<M> mapperClass;

    public GenericFacade() {
        Type genericSuperClass = getClass().getGenericSuperclass();

        ParameterizedType parametrizedType = null;
        while (parametrizedType == null) {
            if ((genericSuperClass instanceof ParameterizedType)) {
                parametrizedType = (ParameterizedType) genericSuperClass;
            } else {
                genericSuperClass = ((Class<?>) genericSuperClass).getGenericSuperclass();
            }
        }

        this.pojoClass = (Class<P>) parametrizedType.getActualTypeArguments()[0];
        this.exampleClass = (Class<E>) parametrizedType.getActualTypeArguments()[2];
        this.mapperClass = (Class<M>) parametrizedType.getActualTypeArguments()[3];
    }

    public SqlSession getSqlSession() throws IOException {
        return MyBatisSqlSessionFactory.openSession();
    }

    protected M getMapper(SqlSession session) {
        try {
            M mapper = session.getMapper(mapperClass);
            return mapper;
        } catch (Exception e) {
            logger.error("", e);
            return null;
        }
    }

    public int countByExample(E example) {
        int result = -1;
        SqlSession session = null;
        try {
            session = this.getSqlSession();
            M mapper = getMapper(session);
            Method m = mapper.getClass().getMethod("countByExample", exampleClass);
            result = (int) m.invoke(mapper, example);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
            logger.error("", e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public int deleteByExample(E example) {
        int result = -1;
        SqlSession session = null;
        try {
            session = this.getSqlSession();
            M mapper = getMapper(session);
            Method m = mapper.getClass().getMethod("deleteByExample", exampleClass);
            result = (int) m.invoke(mapper, example);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
            logger.error("", e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public int deleteByPrimaryKey(ID id) {
        int result = -1;
        SqlSession session = null;
        try {
            session = this.getSqlSession();
            M mapper = getMapper(session);
            Method m = mapper.getClass().getMethod("deleteByPrimaryKey", id.getClass());
            result = (int) m.invoke(mapper, id);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
            logger.error("", e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public int insert(P record) {
        int result = -1;
        SqlSession session = null;
        try {
            session = this.getSqlSession();
            M mapper = getMapper(session);
            Method m = mapper.getClass().getMethod("insert", pojoClass);
            result = (int) m.invoke(mapper, record);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
            logger.error("", e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public int insertSelective(P record) {
        int result = -1;
        SqlSession session = null;
        try {
            session = this.getSqlSession();
            M mapper = getMapper(session);
            Method m = mapper.getClass().getMethod("insertSelective", pojoClass);
            result = (int) m.invoke(mapper, record);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
            logger.error("", e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public List<P> selectByExample(E example) {
        List<P> result = null;
        SqlSession session = null;
        try {
            session = this.getSqlSession();
            M mapper = getMapper(session);
            Method m = mapper.getClass().getMethod("selectByExample", exampleClass);
            result = (List<P>) m.invoke(mapper, example);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
            logger.error("", e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public P selectByPrimaryKey(ID id) {
        P result = null;
        SqlSession session = null;
        try {
            session = this.getSqlSession();
            M mapper = getMapper(session);
            Method m = mapper.getClass().getMethod("selectByPrimaryKey", id.getClass());
            result = (P) m.invoke(mapper, id);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
            logger.error("", e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public int updateByExampleSelective(P record, E example) {
        int result = -1;
        SqlSession session = null;
        try {
            session = this.getSqlSession();
            M mapper = getMapper(session);
            Method m = mapper.getClass().getMethod("updateByExampleSelective", pojoClass, exampleClass);
            result = (int) m.invoke(mapper, record, example);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
            logger.error("", e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public int updateByExample(P record, E example) {
        int result = -1;
        SqlSession session = null;
        try {
            session = this.getSqlSession();
            M mapper = getMapper(session);
            Method m = mapper.getClass().getMethod("updateByExample", exampleClass);
            result = (int) m.invoke(mapper, example);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
            logger.error("", e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public int updateByPrimaryKeySelective(P record) {
        int result = -1;
        SqlSession session = null;
        try {
            session = this.getSqlSession();
            M mapper = getMapper(session);
            Method m = mapper.getClass().getMethod("updateByPrimaryKeySelective", pojoClass);
            result = (int) m.invoke(mapper, record);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
            logger.error("", e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    public int updateByPrimaryKey(P record) {
        int result = -1;
        SqlSession session = null;
        try {
            session = this.getSqlSession();
            M mapper = getMapper(session);
            Method m = mapper.getClass().getMethod("updateByPrimaryKey", pojoClass);
            result = (int) m.invoke(mapper, record);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
            logger.error("", e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
}
