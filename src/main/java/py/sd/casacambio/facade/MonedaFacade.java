/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.casacambio.facade;

import javax.ejb.Stateless;
import py.sd.casacambio.mappers.MonedaMapper;
import py.sd.casacambio.pojos.Moneda;
import py.sd.casacambio.pojos.MonedaExample;

/**
 *
 * @author Miguel Fernandez <myguelfernandez@gmail.com>
 */
@Stateless
public class MonedaFacade extends GenericFacade<Moneda, Long, MonedaExample, MonedaMapper>{
    
}
