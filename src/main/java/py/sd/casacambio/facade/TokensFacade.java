/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.casacambio.facade;

import javax.ejb.Stateless;
import py.sd.casacambio.mappers.TokensMapper;
import py.sd.casacambio.pojos.Tokens;
import py.sd.casacambio.pojos.TokensExample;



/**
 *
 * @author Miguel Fernandez <myguelfernandez@gmail.com>
 */
@Stateless
public class TokensFacade extends GenericFacade<Tokens, Long, TokensExample, TokensMapper>{

    
}
