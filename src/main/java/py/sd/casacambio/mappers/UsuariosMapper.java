package py.sd.casacambio.mappers;

import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import py.sd.casacambio.pojos.Usuarios;
import py.sd.casacambio.pojos.UsuariosExample;

public interface UsuariosMapper {
    @SelectProvider(type=UsuariosSqlProvider.class, method="countByExample")
    int countByExample(UsuariosExample example);

    @DeleteProvider(type=UsuariosSqlProvider.class, method="deleteByExample")
    int deleteByExample(UsuariosExample example);

    @Delete({
        "delete from usuarios",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into usuarios (password, usuario, ",
        "estado)",
        "values (#{password,jdbcType=VARCHAR}, #{usuario,jdbcType=VARCHAR}, ",
        "#{estado,jdbcType=BIT})"
    })
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insert(Usuarios record);

    @InsertProvider(type=UsuariosSqlProvider.class, method="insertSelective")
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insertSelective(Usuarios record);

    @SelectProvider(type=UsuariosSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
        @Result(column="usuario", property="usuario", jdbcType=JdbcType.VARCHAR),
        @Result(column="estado", property="estado", jdbcType=JdbcType.BIT)
    })
    List<Usuarios> selectByExample(UsuariosExample example);

    @Select({
        "select",
        "id, password, usuario, estado",
        "from usuarios",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
        @Result(column="usuario", property="usuario", jdbcType=JdbcType.VARCHAR),
        @Result(column="estado", property="estado", jdbcType=JdbcType.BIT)
    })
    Usuarios selectByPrimaryKey(Integer id);

    @UpdateProvider(type=UsuariosSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Usuarios record, @Param("example") UsuariosExample example);

    @UpdateProvider(type=UsuariosSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Usuarios record, @Param("example") UsuariosExample example);

    @UpdateProvider(type=UsuariosSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Usuarios record);

    @Update({
        "update usuarios",
        "set password = #{password,jdbcType=VARCHAR},",
          "usuario = #{usuario,jdbcType=VARCHAR},",
          "estado = #{estado,jdbcType=BIT}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Usuarios record);
}