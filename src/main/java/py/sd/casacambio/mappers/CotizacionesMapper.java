package py.sd.casacambio.mappers;

import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import py.sd.casacambio.pojos.Cotizaciones;
import py.sd.casacambio.pojos.CotizacionesExample;

public interface CotizacionesMapper {
    @SelectProvider(type=CotizacionesSqlProvider.class, method="countByExample")
    int countByExample(CotizacionesExample example);

    @DeleteProvider(type=CotizacionesSqlProvider.class, method="deleteByExample")
    int deleteByExample(CotizacionesExample example);

    @Delete({
        "delete from cotizaciones",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into cotizaciones (moneda, compra, ",
        "venta)",
        "values (#{moneda,jdbcType=INTEGER}, #{compra,jdbcType=DOUBLE}, ",
        "#{venta,jdbcType=DOUBLE})"
    })
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insert(Cotizaciones record);

    @InsertProvider(type=CotizacionesSqlProvider.class, method="insertSelective")
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insertSelective(Cotizaciones record);

    @SelectProvider(type=CotizacionesSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="moneda", property="moneda", jdbcType=JdbcType.INTEGER),
        @Result(column="compra", property="compra", jdbcType=JdbcType.DOUBLE),
        @Result(column="venta", property="venta", jdbcType=JdbcType.DOUBLE)
    })
    List<Cotizaciones> selectByExample(CotizacionesExample example);

    @Select({
        "select",
        "id, moneda, compra, venta",
        "from cotizaciones",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="moneda", property="moneda", jdbcType=JdbcType.INTEGER),
        @Result(column="compra", property="compra", jdbcType=JdbcType.DOUBLE),
        @Result(column="venta", property="venta", jdbcType=JdbcType.DOUBLE)
    })
    Cotizaciones selectByPrimaryKey(Integer id);

    @UpdateProvider(type=CotizacionesSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Cotizaciones record, @Param("example") CotizacionesExample example);

    @UpdateProvider(type=CotizacionesSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Cotizaciones record, @Param("example") CotizacionesExample example);

    @UpdateProvider(type=CotizacionesSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Cotizaciones record);

    @Update({
        "update cotizaciones",
        "set moneda = #{moneda,jdbcType=INTEGER},",
          "compra = #{compra,jdbcType=DOUBLE},",
          "venta = #{venta,jdbcType=DOUBLE}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Cotizaciones record);
}