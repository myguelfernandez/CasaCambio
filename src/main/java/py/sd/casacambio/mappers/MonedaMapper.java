package py.sd.casacambio.mappers;

import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import py.sd.casacambio.pojos.Moneda;
import py.sd.casacambio.pojos.MonedaExample;

public interface MonedaMapper {
    @SelectProvider(type=MonedaSqlProvider.class, method="countByExample")
    int countByExample(MonedaExample example);

    @DeleteProvider(type=MonedaSqlProvider.class, method="deleteByExample")
    int deleteByExample(MonedaExample example);

    @Delete({
        "delete from moneda",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into moneda (descripcion)",
        "values (#{descripcion,jdbcType=VARCHAR})"
    })
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insert(Moneda record);

    @InsertProvider(type=MonedaSqlProvider.class, method="insertSelective")
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insertSelective(Moneda record);

    @SelectProvider(type=MonedaSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="descripcion", property="descripcion", jdbcType=JdbcType.VARCHAR)
    })
    List<Moneda> selectByExample(MonedaExample example);

    @Select({
        "select",
        "id, descripcion",
        "from moneda",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="descripcion", property="descripcion", jdbcType=JdbcType.VARCHAR)
    })
    Moneda selectByPrimaryKey(Integer id);

    @UpdateProvider(type=MonedaSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Moneda record, @Param("example") MonedaExample example);

    @UpdateProvider(type=MonedaSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Moneda record, @Param("example") MonedaExample example);

    @UpdateProvider(type=MonedaSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Moneda record);

    @Update({
        "update moneda",
        "set descripcion = #{descripcion,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Moneda record);
}