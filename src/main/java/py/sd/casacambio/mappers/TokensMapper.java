package py.sd.casacambio.mappers;

import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import py.sd.casacambio.pojos.Tokens;
import py.sd.casacambio.pojos.TokensExample;

public interface TokensMapper {
    @SelectProvider(type=TokensSqlProvider.class, method="countByExample")
    int countByExample(TokensExample example);

    @DeleteProvider(type=TokensSqlProvider.class, method="deleteByExample")
    int deleteByExample(TokensExample example);

    @Delete({
        "delete from tokens",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into tokens (token, estado, ",
        "usuario)",
        "values (#{token,jdbcType=VARCHAR}, #{estado,jdbcType=BIT}, ",
        "#{usuario,jdbcType=VARCHAR})"
    })
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insert(Tokens record);

    @InsertProvider(type=TokensSqlProvider.class, method="insertSelective")
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insertSelective(Tokens record);

    @SelectProvider(type=TokensSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="token", property="token", jdbcType=JdbcType.VARCHAR),
        @Result(column="estado", property="estado", jdbcType=JdbcType.BIT),
        @Result(column="usuario", property="usuario", jdbcType=JdbcType.VARCHAR)
    })
    List<Tokens> selectByExample(TokensExample example);

    @Select({
        "select",
        "id, token, estado, usuario",
        "from tokens",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="token", property="token", jdbcType=JdbcType.VARCHAR),
        @Result(column="estado", property="estado", jdbcType=JdbcType.BIT),
        @Result(column="usuario", property="usuario", jdbcType=JdbcType.VARCHAR)
    })
    Tokens selectByPrimaryKey(Integer id);

    @UpdateProvider(type=TokensSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Tokens record, @Param("example") TokensExample example);

    @UpdateProvider(type=TokensSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Tokens record, @Param("example") TokensExample example);

    @UpdateProvider(type=TokensSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Tokens record);

    @Update({
        "update tokens",
        "set token = #{token,jdbcType=VARCHAR},",
          "estado = #{estado,jdbcType=BIT},",
          "usuario = #{usuario,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Tokens record);
}