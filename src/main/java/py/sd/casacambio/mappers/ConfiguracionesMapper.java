package py.sd.casacambio.mappers;

import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import py.sd.casacambio.pojos.Configuraciones;
import py.sd.casacambio.pojos.ConfiguracionesExample;

public interface ConfiguracionesMapper {
    @SelectProvider(type=ConfiguracionesSqlProvider.class, method="countByExample")
    int countByExample(ConfiguracionesExample example);

    @DeleteProvider(type=ConfiguracionesSqlProvider.class, method="deleteByExample")
    int deleteByExample(ConfiguracionesExample example);

    @Delete({
        "delete from configuraciones",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into configuraciones (valor, descripcion)",
        "values (#{valor,jdbcType=VARCHAR}, #{descripcion,jdbcType=VARCHAR})"
    })
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insert(Configuraciones record);

    @InsertProvider(type=ConfiguracionesSqlProvider.class, method="insertSelective")
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insertSelective(Configuraciones record);

    @SelectProvider(type=ConfiguracionesSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="valor", property="valor", jdbcType=JdbcType.VARCHAR),
        @Result(column="descripcion", property="descripcion", jdbcType=JdbcType.VARCHAR)
    })
    List<Configuraciones> selectByExample(ConfiguracionesExample example);

    @Select({
        "select",
        "id, valor, descripcion",
        "from configuraciones",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="valor", property="valor", jdbcType=JdbcType.VARCHAR),
        @Result(column="descripcion", property="descripcion", jdbcType=JdbcType.VARCHAR)
    })
    Configuraciones selectByPrimaryKey(Integer id);

    @UpdateProvider(type=ConfiguracionesSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Configuraciones record, @Param("example") ConfiguracionesExample example);

    @UpdateProvider(type=ConfiguracionesSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Configuraciones record, @Param("example") ConfiguracionesExample example);

    @UpdateProvider(type=ConfiguracionesSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Configuraciones record);

    @Update({
        "update configuraciones",
        "set valor = #{valor,jdbcType=VARCHAR},",
          "descripcion = #{descripcion,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Configuraciones record);
}