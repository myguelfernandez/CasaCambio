/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.casacambio;

import java.util.Random;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.naming.InitialContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import py.sd.casacambio.facade.CotizacionesFacade;
import py.sd.casacambio.facade.TokensFacade;
import py.sd.casacambio.facade.UsuariosFacade;
import py.sd.casacambio.pojos.Cotizaciones;
import py.sd.casacambio.pojos.Tokens;
import py.sd.casacambio.pojos.TokensExample;
import py.sd.casacambio.pojos.UsuariosExample;

/**
 *
 * @author Miguel Fernandez <miguel.fernandez@konecta.com.py>
 */
@WebService(serviceName = "CasaCambioWs")
public class CasaCambioWs {

    private final Logger logger = LogManager.getLogger(this.getClass());

    InitialContext ctx;
    UsuariosFacade usuariosFacade;
    TokensFacade tokensFacade;
    CotizacionesFacade cotizacionesFacade;

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "login")
    public LoginRespuesta login(@WebParam(name = "user") String user, @WebParam(name = "pass") String pass) {
        LoginRespuesta resp = new LoginRespuesta();
        logger.info("user: " + user + " pass: " + pass);
        try {

            ctx = new InitialContext();
            usuariosFacade = (UsuariosFacade) ctx.lookup("java:global/CasaCambio/UsuariosFacade");
            tokensFacade = (TokensFacade) ctx.lookup("java:global/CasaCambio/TokensFacade");

            //Verificar que exita el usuario
            UsuariosExample example = new UsuariosExample();
            example.createCriteria()
                    .andUsuarioEqualTo(user)
                    .andPasswordEqualTo(pass)
                    .andEstadoEqualTo(Boolean.TRUE);

            int count = usuariosFacade.countByExample(example);

            if (count > 0) {
                Random rand = new Random(System.currentTimeMillis());
                //generar token
                Integer token = rand.nextInt(99999999) + 100000;
                Tokens tokenRecord = new Tokens();
                tokenRecord.setEstado(Boolean.TRUE);
                tokenRecord.setToken(String.valueOf(token));
                tokenRecord.setUsuario(user);
                int tokenInseratdo = tokensFacade.insert(tokenRecord);
                if (tokenInseratdo > 0) {
                    resp.setEstado(0);
                    resp.setMensaje("OK");
                    resp.setToken(String.valueOf(token));
                } else {
                    resp.setEstado(-1);
                    resp.setMensaje("error al generar y guardar el token, Intentelo de nuevo");
                }
            } else {
                resp.setEstado(-1);
                resp.setMensaje("usuario no existe o no esta activo");
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resp.setEstado(-1);
            resp.setMensaje(e.getMessage());
        }
        logger.info("OUT: " + resp);
        return resp;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "compra")
    public CambioRespuesta compra(
            @WebParam(name = "moneda") Integer moneda,
            @WebParam(name = "monto") Double monto,
            @WebParam(name = "token") String token) {
        logger.info("IN: [{}]", moneda, monto, token);
        CambioRespuesta resp = new CambioRespuesta();
        try {
            //verificar token recibido con el guardado anteriormente en base de datos
            ctx = new InitialContext();
            tokensFacade = (TokensFacade) ctx.lookup("java:global/CasaCambio/TokensFacade");
            cotizacionesFacade = (CotizacionesFacade) ctx.lookup("java:global/CasaCambio/CotizacionesFacade");

            //verificamos token
            TokensExample tokensExample = new TokensExample();
            tokensExample.createCriteria()
                    .andTokenEqualTo(token)
                    .andEstadoEqualTo(Boolean.TRUE);
            int tokenCount = tokensFacade.countByExample(tokensExample);

            if (tokenCount > 0) {
                
                //consultamos cotizacion de moneda
                Cotizaciones cotizacion = cotizacionesFacade.selectByPrimaryKey(moneda);
                monto = monto * cotizacion.getCompra();
                //implementar logica para realizar el cambio de moneda a guaranies segun el tipo de moneda recibido
                resp.setEstado(0);
                resp.setMensaje("OK");
                resp.setMonto(monto);

                //Hacemos expirar el token
                Tokens tokens = new Tokens();
                tokens.setToken(token);
                tokens.setEstado(Boolean.FALSE);
                tokensFacade.updateByExampleSelective(tokens, tokensExample);
            } else {
                resp.setEstado(-1);
                resp.setMensaje("tokens no valido");
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resp.setEstado(-1);
            resp.setMensaje(e.getMessage());
        }
        logger.info("OUT: " + resp);
        return resp;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "venta")
    public CambioRespuesta venta(
            @WebParam(name = "moneda") Integer moneda,
            @WebParam(name = "monto") Double monto,
            @WebParam(name = "token") String token) {
        logger.info("IN: [{}]", moneda, monto, token);
        CambioRespuesta resp = new CambioRespuesta();
        try {
            //verificar token recibido con el guardado anteriormente en base de datos
            ctx = new InitialContext();
            tokensFacade = (TokensFacade) ctx.lookup("java:global/CasaCambio/TokensFacade");
            cotizacionesFacade = (CotizacionesFacade) ctx.lookup("java:global/CasaCambio/CotizacionesFacade");

            //verificamos token
            TokensExample tokensExample = new TokensExample();
            tokensExample.createCriteria()
                    .andTokenEqualTo(token)
                    .andEstadoEqualTo(Boolean.TRUE);
            int tokenCount = tokensFacade.countByExample(tokensExample);

            if (tokenCount > 0) {
                
                //consultamos cotizacion de moneda
                Cotizaciones cotizacion = cotizacionesFacade.selectByPrimaryKey(moneda);
                monto = monto * cotizacion.getVenta();

                //implementar logica para realizar el cambio de moneda a guaranies segun el tipo de moneda recibido
                resp.setEstado(0);
                resp.setMensaje("OK");
                resp.setMonto(monto);

                //Hacemos expirar el token
                Tokens tokens = new Tokens();
                tokens.setToken(token);
                tokens.setEstado(Boolean.FALSE);
                tokensFacade.updateByExampleSelective(tokens, tokensExample);
            } else {
                resp.setEstado(-1);
                resp.setMensaje("tokens no valido");
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resp.setEstado(-1);
            resp.setMensaje(e.getMessage());
        }
        logger.info("OUT: " + resp);
        return resp;
    }

}
