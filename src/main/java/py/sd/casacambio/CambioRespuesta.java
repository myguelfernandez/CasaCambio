/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.casacambio;

/**
 *
 * @author Miguel Fernandez <miguel.fernandez@konecta.com.py>
 */
public class CambioRespuesta extends BaseRespuesta{
    Double monto;

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    @Override
    public String toString() {
        return "CambioRespuesta{" + "monto=" + monto + '}'+super.toString();
    }
    
}
