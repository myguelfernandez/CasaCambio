/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.casacambio;

/**
 *
 * @author Miguel Fernandez <miguel.fernandez@konecta.com.py>
 */
public class LoginRespuesta extends BaseRespuesta{
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "LoginRespuesta{" + "token=" + token + '}'+super.toString();
    }
    
    
}
